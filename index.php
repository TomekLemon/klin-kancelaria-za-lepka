<!DOCTYPE html>
<html>
	<head>
		<title>Klin Kancelaria - strona w budowie</title>
		<link rel="stylesheet" href="assets/css/style.css"/>
		<link href="https://fonts.googleapis.com/css?family=Lato:400,900&amp;subset=latin-ext" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<img src="assets/images/logo.png" class="brand-logo" alt="klin kancelaria" />
			<h1 class="main-header">Strona w budowie</h1>
			<p>Zapraszamy wkrótce</p>
			<img src="assets/images/lemonade-studio-logo.png" class="lemo-logo" alt="lemonade studio" />
		</div>
	</body>
</html>